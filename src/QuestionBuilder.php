<?php

namespace omeripek\Questionbuilder;

class QuestionBuilder {
	
	public static $RandomQuestionText = array(
		'Which one of them?',
		'What is this?',
		'Who is this?',
		'Which one is correct team name in NBA?',
		'What animal would be cutest if scaled down to the size of a cat?',
		'What weird food combinations do you really enjoy?',
		'Where do you get your news?',
		'What is wrong but sounds right?',
		'Who do you go out of your way to be nice to?',
		'What food is delicious but a pain to eat?',
		'What (old person) things do you do?',
		'What was the last photo you took?',
		'Where are some unusual places you have been?',
		'What is the spiciest thing you have ever eaten?',
		'What is the most expensive thing you’ve broken?',
		'What riddles do you know?',
		'What is your cure for hiccups?',
		'Do you think that aliens exist?',
		'What are you currently worried about?',
		'How do you judge a person?',
		'What movie quotes do you use on a regular basis?',
		'What is the funniest joke you know by heart?',
		'What is the most ironic thing you’ve seen happen?',
		'What game have you spent the most hours playing?',
    	'What is the most comfortable bed or chair you have ever been in?',
    	'What is the craziest conversation you have overheard?',
    	'What is the hardest you have ever worked?',
	);

	public static $RandomAnswers = array(
		'Test',
		'An apple',
		'Orange',
		'Huston Rocket',
		'İstanbul',
		'Ankara',
		'New York',
		'Yellow',
		'Cats',
		'Arrow',
		'Funny',
		'Photo',
		'Nobody',
		'None',
		'Rocket',
		'Galatasaray',
		'Turkey',
		'Kartal',
		'Worry',
		'No judge',
		'Person of',
		'Logan',
		'Jack',
		'Playing Game',
    	'Comfortable',
    	'Craziest conversation',
    	'Hardest',
    	'Answer A',
    	'Answer B',
    	'Answer C',
    	'Answer D',
    	'Answer E',
    	'Answer 1',
    	'Answer 2',
    	'Answer 3',
    	'Answer 4',
    	'Answer 5',
	);

	public static $RandomCorrectAnswer = array(
		1,
		2,
		3,
	);

}